# Amoebas CI
A program for computing amoebas of bivariate polynomials by means of contour integration.

## Compiling
Can be compiled from Lazarus ('Release' build mode) or by using `fpc -O4 -Fo ./ Amoebas.lpr` command. Additional performance can be gained under Linux by uncommenting `$DEFINE ASM` directive.

## Usage
The parameters are supplied via `Config.ini`.
| Parameter | Description |
| ------ | ------ |
|PathPoly| Path to the input polynomial.|
|PathAmoeba| Path to the output files. |
|ImgSize| Image size in pixels. Will be rounded to 1 + the nearest power of 2.|
|RegionSize| The amoeba will be built in the [-RegionSize, RegionSize]^2 region. |
|NAngles| The number of angular sections. Should preferably be a power of 2. Recommended values: 2^4 -- 2^10.|
|NIntPoints| The maximal number of integration points. Recommended values: 2^10 -- 2^17.|
|Epsilon| Numerical integration tolerance. Recommended values: 2^-2 -- 2^-7. |.
|WorkMode| Normal / TestAcc / TestScaling. |

The input polynomial should be provided as tab-separated data. Each line should represent a single term using the format _x exponent -- y exponent -- Re(coefficient) -- Im(coefficient)_ (see the included polynomials).

### Normal mode
Compute the amoeba using the config parameters. The results are saved as two files: PathAmoeba.pgm (binary image) and PathAmoeba.dat (full data containing component orders and integration point counts as 32-bit integers).

### TestAcc mode
Compute the amoeba using different combinations of _NAngles, NIntPoints_, and _Epsilon_.

### TestScaling mode
Compute the amoeba with different image sizes using the single-scale and the multi-scale versions of the algorithm. 

## License
The project is distributed under the fair license, see `Amoebas.lpr`.
