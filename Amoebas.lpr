{ 
Copyright (c) Peter Karpov 2022.

Usage of the works is permitted provided that this instrument is retained with 
the works, so that any entity that uses the works is notified of this instrument.

DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.
}
{$MODE OBJFPC} {$COPERATORS ON} {$ASMMODE INTEL} {$PIC OFF} {.$DEFINE ASM}
program Amoebas; ///////////////////////////////////////////////////////////////
{
>> Version: 1.0

>> Description
   A program for computing amoebas of bivariate polynomials.
   
>> Author
   Peter Karpov
   Email    : PeterKarpov@inversed.ru
   Homepage : inversed.ru
   GitHub   : inversed-ru
   GitLab   : inversed
   Twitter  : @inversed_ru
   
>> Changelog
   1.0 -- 2022.12.28 
      - Dependency from uComplex unit
      ~ Early exit from component index calculation
      + Saving amoebas as PBM files
      + Config file support
      * Incorrect integration point count (last instead of total)
   0.7 -- 2022.06.20 
      + Routines for testing performance and acuraccy
   0.6 -- 2022.06.06 
      ~ Back to v0.4 evaluation
      * Speedup: differentiation performed after conversion to univariate form
      + Convex acceleration levels
      + Tracking of the number of numeric integration points
      * Convex-accelerated regions are now filled with proper component indices 
        instead of zeros
      + ASM switch for assembly code
      * ToOrder1D returning identical values for different orders
   0.5 -- 2022.06.02 
      + Experimental accelerated evaluation scheme
   0.4 -- 2022.05.30 
      + Horner scheme written in ASM
   0.0 -- 2022.05.19
      + Initial version
   Notation: + added, - removed, * fixed, ~ changed   

>> ToDo
   ? Get rid of some InvLibs dependencies
   ? ASM Horner with derivative evaluation
   - Polynomial rescaling for improved stability
   - SDL2 visualization
   - Multithreading
   ? Faster conversion to univariate form
   ? FFT or more sophisticated integration
}

uses
   Sysutils,
   InvSys,
   Arrays,
   Math,
   ExtraMath,
   Formatting,
   StringLists,
   IniConfigs;

const
   Dim = 2;
   AmoebaInterior = Low(Integer);
   InvalidComponent = -1;
   ConvexAccel = 3; // 0 - none, 1 - conservative, 2 - normal, 3 - agressive
   
type
   Complex = 
      record
      Re, Im   :  Real;
      end;
      
   PComplex = ^Complex;

   TPolyTerm =
      record
      p  :  array [0 .. Dim - 1] of Integer;
      c  :  Complex;
      end;
      
   TPoly = array of TPolyTerm;
   
   TPolys = array [0 .. Dim - 1] of TPoly;
   
   TComplexArray = array of Complex;
   
   TMultiPowers = array [0 .. Dim - 1] of TComplexArray;
   
   TMultiVar = array [0 .. Dim - 1] of Complex;
   TMultiVars = array of array [0 .. Dim - 1] of Complex;
   
   TAmoebaCoords = array [0 .. Dim - 1] of Real;

   TAmoebaPixel =
      record
      NPoints,
      Order1D  :  Integer;
      end;

   TAmoeba = array of array of TAmoebaPixel;
   
   TConfigParams = 
      record
      PathPoly,
      PathAmoeba  :  AnsiString;
      ImgSize,
      NAngles,
      NIntPoints  :  Integer;
      RegionSize,
      Epsilon     :  Real;
      WorkMode    :  AnsiString;
      end;

const
   mI :  Complex = (Re : 0.0; Im : 1.0);
   _0 :  Complex = (Re : 0.0; Im : 0.0);
   
{-----------------------<< Complex Numbers >>----------------------------------}
// Based on uComplex unit by Pierre Muller. The main reason for avoiding 
// uComplex is the fact that the imaginary unit is called i, and I'm not giving
// up my loop identifiers.

operator := (r: Real) z: Complex; inline;
   begin
   z.Re := r;
   z.Im := 0.0;
   end;
   
operator + (z1, z2: Complex) z: Complex; inline;
   begin
   z.Re := z1.Re + z2.Re;
   z.Im := z1.Im + z2.Im;
   end;
   
operator - (z1, z2: Complex) z: Complex; inline;
   begin
   z.Re := z1.Re - z2.Re;
   z.Im := z1.Im - z2.Im;
   end;
   
operator * (z1, z2: Complex) z: Complex; inline;
   begin
   z.Re := z1.Re * z2.Re - z1.Im * z2.Im;
   z.Im := z1.Re * z2.Im + z1.Im * z2.Re;
   end;
   
operator * (z1: Complex; r: Real) z: Complex; inline;
   begin
   z.Re := z1.Re * r;
   z.Im := z1.Im * r;
   end;
   
operator / (znum, zden: Complex) z: Complex; inline;
   var
      t, Denom : Real;
   begin
   if ( abs(zden.Re) > abs(zden.Im) ) then
      begin
      t := zden.Im / zden.Re;
      Denom :=  zden.Re + zden.Im * t;
      z.Re  := (znum.Re + znum.Im * t) / Denom;
      z.Im  := (znum.Im - znum.Re * t) / Denom;
      end 
   else
      begin
      t := zden.Re / zden.Im;
      Denom :=   zden.Im + zden.Re * t;
      z.Re  := ( znum.Im + znum.Re * t) / Denom;
      z.Im  := (-znum.Re + znum.Im * t) / Denom;
      end;
   end;
   
// Complex modulus
function CAbs(
      z  : Complex
      )  : Real;
   begin
   with z do
      Result := Sqrt(Re * Re + Im * Im);
   end;
   
   
// Complex exponential
function CExp(
         z     : Complex
         )     : Complex;
   var 
         expz  : Real;
   begin
   expz := exp(z.Re);
   Result.Re := expz * cos(z.Im);
   Result.Im := expz * sin(z.Im);
   end;
   
{-----------------------<< Polynomials >>--------------------------------------}

// Load Poly from a file at a given Path
procedure LoadPoly(
   var   Poly     :  TPoly;
   const Path     :  AnsiString);
   var
         FilePoly :  Text;
         n, k     :  Integer;
   begin
   OpenRead(FilePoly, Path);
   n := 0;
   repeat
      Inc(n);
      if Length(Poly) < n then
         SetLength(Poly, 2 * n);
      with Poly[n - 1] do
         begin
         for k := 0 to Dim - 1 do
            Read(FilePoly, p[k]);
         ReadLn(FilePoly, c.Re, c.Im);
         end;
   until EoF(FilePoly);
   Close(FilePoly);
   SetLength(Poly, n);
   end;
   
   
// Calculate the powers of x from 0 to MaxP
procedure CalcPowers(
   var   Powers   :  TComplexArray;
         x        :  Complex;
         MaxP     :  Integer;
         Init     :  Boolean);
   var
         i        :  Integer;
   begin
   if Init then
      SetLength(Powers, MaxP + 1);
   Powers[0] := 1;
   for i := 1 to MaxP do
      Powers[i] := x * Powers[i - 1];
   end;
   
   
// Return the maximal powers of Poly's terms
procedure FindMaxPowers(
   var   MaxP  :  TIntArray;
   const Poly  :  TPoly);
   var
         i, j  :  Integer;
   begin
   InitArray(MaxP, Dim, {Value:} 0);
   for i := 0 to High(Poly) do
      for j := 0 to Dim - 1 do
         MaxP[j] := Max(MaxP[j], Poly[i].p[j]);
   end;

   
// Evaluate Poly at z given the precalculated powers MP
function EvalPoly(
   const Poly  :  TPoly;
   const MP    :  TMultiPowers
         )     :  Complex;
   var
         i     :  Integer;
         Sum   :  Complex;
   begin
   Sum := _0;
   for i := 0 to High(Poly) do
      with Poly[i] do
         Sum += c * MP[ 0, p[0] ] * MP[ 1, p[1] ];
   Result := Sum;
   end;


procedure ToUnivariate(
   var   Coeffs   :  TComplexArray;
   const Poly     :  TPoly;
   const MaxP     :  TIntArray;
   const z        :  TMultiVar;
         IdVar    :  Integer);
   var
         Powers   :  TComplexArray;
         i, j     :  Integer;
         q        :  Complex;
   begin
   CalcPowers(Powers, z[1 - IdVar], MaxP[1 - IdVar], {Init:} True);
   SetLength(Coeffs, 1 + MaxP[IdVar]);
   for i := 0 to High(Coeffs) do
      Coeffs[i] := _0;
   for i := 0 to High(Poly) do
      with Poly[i] do
         begin
         j := p[IdVar];
         Coeffs[j] += c * Powers[ p[1 - IdVar] ];
         end;
   end;

   
// Differentiate Poly wrt to the variable IdVar
procedure PartialDiff(
   var   DPoly :  TPoly;
   const Poly  :  TPoly;
         IdVar :  Integer);
   var
         i, n  :  Integer;
   begin
   DPoly := Copy(Poly);
   i := 0;
   n := Length(Poly);
   repeat
      DPoly[i].c *= DPoly[i].p[IdVar];
      DPoly[i].p[IdVar] -= 1;
      if DPoly[i].p[IdVar] < 0 then
         begin
         n -= 1;
         DPoly[i] := DPoly[n];
         end
      else
         i += 1;
   until i >= n;
   SetLength(DPoly, n);
   end;
   
   
// Return all the partial derivatives of Poly
procedure FullDiff(
   var   DPolys   :  TPolys;
   const Poly     :  TPoly);
   var
         i        :  Integer;
   begin
   for i := 0 to Dim - 1 do
      PartialDiff(DPolys[i], Poly, i);
   end;

{-----------------------<< Univariate Polynomials >>---------------------------}

// Evaluate a polynomial defined by Coeffs at x using Horner scheme
function EvalHorner(
   const Coeffs   :  TComplexArray;
         x        :  Complex
         )        :  Complex;
   var
         S, Sn    :  Complex;
         i, h     :  Integer;
   begin
   h := High(Coeffs);
   S := Coeffs[h];
   for i := h - 1 downto 0 do
      begin
      Sn.Re := Coeffs[i].Re + x.re * S.re - x.im * S.im;
      Sn.Im := Coeffs[i].Im + x.re * S.im + x.im * S.re;
      S := Sn;
      end;
   Result := S;
   end;


// Evaluate a polynomial defined by Coeffs and its derivative 
// at x using Horner scheme
procedure EvalHorner2(
   var   P, dP    :  Complex;
   const Coeffs   :  TComplexArray;
         x        :  Complex);
   var
         Pn, dPn  :  Complex;
         i, h     :  Integer;
   begin
   h := High(Coeffs);
   P := Coeffs[h];
   dP := _0;
   for i := h - 1 downto 0 do
      begin
      dPn.Re := P.Re + x.Re * dP.Re - x.Im * dP.Im;
      dPn.Im := P.Im + x.Re * dP.Im + x.Im * dP.Re;
      dP := dPn;
      Pn.Re := Coeffs[i].Re + x.Re * P.Re - x.Im * P.Im;
      Pn.Im := Coeffs[i].Im + x.Re * P.Im + x.Im * P.Re;
      P := Pn;
      end;
   end;


{$IFDEF ASM}
// Evaluate a polynomial defined by an array of coefficients starting at 
// PtrFirstC and ending at PtrLastC at the point PtrX^ using Horner scheme.
// Does not work on Windows due to ABI differences.
function EvalHornerAsm(
         PtrFirstC,
         PtrLastC,
         PtrX        :  PComplex
         )           :  Complex;
         assembler;
   label
         Loop;
   const
         Negator     :  Int64 = $8000000000000000;
   asm
   // xmm0  S.Re, S.Im
   // xmm1  S.Im, S.Re
   // xmm2  x.Re, x.Re
   // xmm3 -x.Im, x.Im

   MOVAPD   xmm0     ,  [PtrLastC]     // S := Coeffs[L - 1]
   SUB      PtrLastC ,  16             // Dec(i)
   MOVDDUP  xmm2     ,  [PtrX]
   MOVDDUP  xmm3     ,  [PtrX + 8]
   MOVQ     xmm4     ,  Negator
   PXOR     xmm3     ,  xmm4           // xmm3 := -x.Im, x.Im

   Loop:
   MOVAPD   xmm1     ,  xmm0
   SHUFPD   xmm1     ,  xmm1  ,  1     // xmm1 := S.Im, S.Re
   MULPD    xmm1     ,  xmm3
   MOVAPD   xmm4     ,  xmm2
   MULPD    xmm4     ,  xmm0
   ADDPD    xmm1     ,  xmm4
   ADDPD    xmm1     ,  [PtrLastC]
   MOVAPD   xmm0     ,  xmm1

   SUB      PtrLastC ,  16             // Dec(i)
   CMP      PtrLastC ,  PtrFirstC
   JGE      Loop                       // Until i < 0
   MOVAPD   Result   ,  xmm0           // Result := S
   end;
{$ENDIF}


// Differentiate a univariate polynomial with given coefficients.
// DCoeffs is initialized if Init is set.
procedure Differentiate(
   var   DCoeffs  :  TComplexArray;
   const  Coeffs  :  TComplexArray;
         Init     :  Boolean);
   var
         i, L     :  Integer;
   begin
   L := Length(Coeffs);
   if Init then
      SetLength(DCoeffs, L - 1);
   for i := 0 to L - 2 do
      DCoeffs[i] := (1 + i) * Coeffs[1 + i];
   end;

{-----------------------<< Component Orders >>---------------------------------}

// Calculate the component suborder with index IdVar of Poly at UV 
// using a slice at specified Angle and NPoints for numeric integration
function CalcSubOrder(
   const UPoly,
         UDPoly      :  TComplexArray;
   const UV          :  TAmoebaCoords;
   const MaxP        :  TIntArray;
         IdVar       :  Integer;
         NPoints     :  Integer;
         Angle,
         Shift       :  Real
         )           :  Complex;
   var
         i, HighUPoly,
         HighUDPoly  :  Integer;
         Sum, z, dz,
         P, dP       :  Complex;
   begin
   Sum := _0;
   z := cexp( UV[    IdVar] + mI * mTau * (Shift / NPoints) );
   dz := cexp( mI * (mTau / NPoints) );
   HighUPoly  := High(UPoly);
   HighUDPoly := High(UDPoly);
   for i := 0 to NPoints - 1 do
      begin
      {$IFDEF ASM}
       P := EvalHornerAsm(@ UPoly[0], @ UPoly[HighUPoly ], @z);
      dP := EvalHornerAsm(@UDPoly[0], @UDPoly[HighUDPoly], @z);
      {$ELSE}
      //Sum += EvalHorner(UDPoly, z) /
      //       EvalHorner( UPoly, z) * z;
      EvalHorner2(P, dP, UPoly, z);
      Sum += dP / P * z;
      {$ENDIF}
      z *= dz;
      end;
   Result := Sum / NPoints;
   end;
   
   
// Calculate the component suborder with index IdVar of Poly at UV 
// using a slice at specified Angle and at most MaxNPoints for 
// adaptive numeric integration. Return the number of integration points.
function CalcAdaptSubOrder(
   var   SubOrder    :  Complex;
   const Poly        :  TPoly;
   const UV          :  TAmoebaCoords;
   const MaxP        :  TIntArray;
         IdVar       :  Integer;
         Epsilon     :  Real;
         MaxNPoints  :  Integer;
         Angle       :  Real
         )           :  Integer;
   var
         n           :  Integer;
         I1, I2      :  Complex;
         z           :  TMultiVar;
         UPoly,
         UDPoly      :  TComplexArray;
   begin
   z[1 - IdVar] := cexp(UV[1 - IdVar] + mI * Angle);
   ToUnivariate(UPoly, Poly, MaxP, z, IdVar);
   Differentiate(UDPoly, UPoly, {Init:} True);
   n := 2;
   I1 := CalcSubOrder(UPoly, UDPoly, UV, MaxP, IdVar, n, Angle, {Shift:} 0);
   I2 := CalcSubOrder(UPoly, UDPoly, UV, MaxP, IdVar, n, Angle, {Shift:} 0.5);
   while (CAbs(I1 - I2) > Epsilon) and (2 * n < MaxNPoints) do
      begin
      n *= 2;
      I1 := 0.5 * (I1 + I2);
      I2 := CalcSubOrder(UPoly, UDPoly, UV, MaxP, IdVar, n, Angle, {Shift:} 0.5);
      end;
   SubOrder := 0.5 * (I1 + I2);
   Result := 2 * n;
   end;
   
   
// Calculate the component order of Poly at z using a slice at specified Angle
// and NPoints for numeric integration
procedure CalcOrder(
   var   Order       :  TMultiVar;
   const Poly        :  TPoly;
   const UV          :  TAmoebaCoords;
   const MaxP        :  TIntArray;
         NPoints     :  Integer;
         Angle       :  Real);
   var
         IdVar       :  Integer;
         UPoly,
         UDPoly      :  TComplexArray;
         z           :  TMultiVar;
   begin
   for IdVar := 0 to Dim - 1 do
      begin
      z[1 - IdVar] := cexp(UV[1 - IdVar] + mI * Angle);
      ToUnivariate(UPoly, Poly, MaxP, z, IdVar);
      Differentiate(UDPoly, UPoly, {Init:} True);
      Order[IdVar] := CalcSubOrder(UPoly, UDPoly, UV, MaxP, IdVar, NPoints, Angle, {Shift:} 0);
      end;
   end;
   
   
function CalcAdaptOrder(
   var   Order       :  TMultiVar;
   const Poly        :  TPoly;
   const UV          :  TAmoebaCoords;
   const MaxP        :  TIntArray;
         Epsilon     :  Real;
         MaxNPoints  :  Integer;
         Angle       :  Real
         )           :  Integer;
   var
         k, NPoints  :  Integer;
   begin
   NPoints := 0;
   for k := 0 to Dim - 1 do
      NPoints += CalcAdaptSubOrder(Order[k], Poly, UV, MaxP, k, Epsilon, MaxNPoints, Angle);
   Result := NPoints;
   end;
   

// Transform a 2D component order into a 1D index
function ToOrder1D(
         x, y     :  Integer
         )        :  Integer;
   var
         i, m, S  :  Integer;
   begin
   S := 0;
   m := 1;
   for i := 0 to 15 do
      begin
      S += (x and m) shl i + (y and m) shl (i + 1);
      m := m shl 1;
      end;
   Result := S;
   end;


function SameComponent(
         a, b  :  Integer
         )     :  Integer;
   begin
   if (a = b) and (a <> AmoebaInterior) then
      Result := a else
      Result := InvalidComponent;
   end;


function SameComponent(
         a, b, c  :  Integer
         )        :  Integer;
   begin
   if (a = b) and (b = c) and (a <> AmoebaInterior) then
      Result := a else
      Result := InvalidComponent;
   end;


function SameComponent(
         a, b, c, d  :  Integer
         )           :  Integer;
   begin
   if (a = b) and (b = c) and (c = d) and (a <> AmoebaInterior) then
      Result := a else
      Result := InvalidComponent;
   end;


// Pick a valid component from a, b or return InvalidComponent if there is none
function PickValidComponent(
         a, b  :  Integer
         )     :  Integer;
   begin
   if a = InvalidComponent then
      Result := b else
      Result := a;
   end;

{-----------------------<< Amoebas - General Routines >>-----------------------}

// Dest := Src
procedure CopyAmoeba(
   var   Dest  :  TAmoeba;
   const Src   :  TAmoeba);
   var
         i     :  Integer;
   begin
   SetLength(Dest, Length(Src));
   for i := 0 to High(Dest) do
      Dest[i] := Copy(Src[i]);
   end;
   

// Return Amoeba dimensions
procedure GetSizes(
   var   SX, SY   :  Integer;
   const Amoeba   :  TAmoeba);
   begin
   SY := Length(Amoeba);
   SX := Length(Amoeba[0]);
   end;


// Get the coordinates of the pixel (i, j)
procedure GetPixelCoords(
   var   uv       :  TAmoebaCoords;
         i, j,
         ImgSize  :  Integer;
         R        :  Real);
   begin
   uv[0] := Blend(-R, R, i / (ImgSize - 1) );
   uv[1] := Blend(-R, R, j / (ImgSize - 1) );
   end;
   
   
// Save Amoeba to a given Path
procedure SaveAmoeba(
   const Path        :  AnsiString;
   const Amoeba      :  TAmoeba;
         SaveFullData:  Boolean);
   var
         FileImage   :  Text;
         FileData    :  file of Integer;
         i, j        :  Integer;
   const
         Ext         =  '.pgm';
         SuffixNP    =  '.dat';
   begin
   // Save the image
   OpenWrite(FileImage, Path + Ext);
   Write(FileImage, 'P5 ', Length(Amoeba[0]), ' ', Length(Amoeba), ' ', High(Byte), ' ');
   for j := Length(Amoeba) - 1 downto 0 do
      for i := 0 to Length(Amoeba[j]) - 1 do
         Write( FileImage, AnsiChar( Ord(Amoeba[j, i].Order1D <> AmoebaInterior) * High(Byte) ) );
   Close(FileImage);

   // Save the orders and the integration point counts
   if SaveFullData then
      begin
      Assign(FileData, Path + SuffixNP);
      Rewrite(FileData);
      for j := 0 to Length(Amoeba) - 1 do
         for i := 0 to Length(Amoeba[j]) - 1 do
            with Amoeba[j, i] do
               Write(FileData, Order1D, NPoints);
      Close(FileData);
      end;
   end;


// Return the Hamming distance between the orders of A and B
function AmoebaDifference(
   const A, B        :  TAmoeba
         )           :  Integer;
   var
         i, j, S     :  Integer;
   begin
   S := 0;
   for j := 0 to Length(A) - 1 do
      for i := 0 to Length(A[j]) - 1 do
         S += Ord(A[j, i].Order1D <> B[j, i].Order1D);
   Result := S;
   end;


// Return the total number of integration points used for computing A
function TotalIntPoints(
   const A           :  TAmoeba
         )           :  Int64;
   var
         i, j        :  Integer;
         S           :  Int64;
   begin
   S := 0;
   for j := 0 to Length(A) - 1 do
      for i := 0 to Length(A[j]) - 1 do
         S += A[j, i].NPoints;
   Result := S;
   end;

{-----------------------<< Amoeba Construction >>------------------------------}
   
// Base-2 van der Corput sequence
function VanDerCorput2(
         n     :  Integer
         )     :  Real;
   var
         S, p  :  Real;
   begin
   S := 0;
   p := 0.5;
   while n > 0 do
      begin
      S += (n and 1) * p;
      n := n shr 1;
      p /= 2;
      end;
   Result := S;
   end;
   
   
// Calculate a single amoeba pixel. Adaptive integration is employed if Epsilon > 0.
procedure CalcAmoebaPixel(
   var   Pixel       :  TAmoebaPixel;
   const Poly        :  TPoly;
   const DPolys      :  TPolys;
   const uv          :  TAmoebaCoords;
   const MaxP        :  TIntArray;
         Epsilon     :  Real;
         MaxNPoints,
         NAngles     :  Integer);
   var
         i, k        :  Integer;
         Orders      :  TMultiVars;
         Angle,
         MaxDev      :  Real;
   begin
   Pixel.NPoints := 0;
   SetLength(Orders, NAngles);
   for i := 0 to NAngles - 1 do
      begin
      Angle := mTau * VanDerCorput2(i);
      if Epsilon > 0 then
         Pixel.NPoints += CalcAdaptOrder(Orders[i], Poly, uv, MaxP, Epsilon, MaxNPoints, Angle)
      else
         begin
         Pixel.NPoints += MaxNPoints;
         CalcOrder(Orders[i], Poly, uv, MaxP, MaxNPoints, Angle);
         end;
      if i > 0 then
         for k := 0 to Dim - 1 do
            if Round(Orders[i, k].Re) <> Round(Orders[i - 1, k].Re) then
               begin
               Pixel.Order1D := AmoebaInterior;
{<}            exit;
               end;
      end;
   Pixel.Order1D := ToOrder1D(Round(Orders[0, 0].Re), Round(Orders[0, 1].Re));
   end;
   
   
// Calculate the Amoeba of Poly on the interval [-R .. R]^2
// with a given size in pixels. Baseline single-resolution algorithm.
procedure CalcAmoeba(
   var   Amoeba   :  TAmoeba;
   const Poly     :  TPoly;
         R        :  Real;
         ImgSize  :  Integer;
         Epsilon  :  Real;
         NPoints,
         NAngles  :  Integer);
   var
         MaxP     :  TIntArray;
         DPolys   :  TPolys;
         uv       :  TAmoebaCoords;
         i, j     :  Integer;
         OldExMask:  TFPUExceptionMask;
   begin
   FindMaxPowers(MaxP, Poly);
   FullDiff(DPolys, Poly);
   OldExMask := GetExceptionMask();
   SetExceptionMask(OldExMask + [exUnderflow, exOverflow, exPrecision, exInvalidOp]);
   SetLength(Amoeba, ImgSize, ImgSize);
   for j := 0 to ImgSize - 1 do
      for i := 0 to ImgSize - 1 do
         begin
         GetPixelCoords(uv, i, j, ImgSize, R);
         CalcAmoebaPixel(Amoeba[j, i], Poly, DPolys, uv, MaxP, Epsilon, NPoints, NAngles);
         end;
   SetExceptionMask(OldExMask);
   end;


// Scale LowRes by a factor of 2. Even-indexed pixels of HiRes are
// filled with LowRes's values, the rest are undefined.
procedure UpscaleAmoeba(
   var   HiRes       :  TAmoeba;
   const LowRes      :  TAmoeba);
   var
         i, j, SX, SY:  Integer;
   begin
   // Initialize the new array
   GetSizes(SX, SY, LowRes);
   SetLength(HiRes, 2 * SY - 1);
   for j := 0 to High(HiRes) do
      SetLength(HiRes[j], 2 * SX - 1);

   // Fill the values
   for j := 0 to SY - 1 do
      for i := 0 to SX - 1 do
         HiRes[2 * j, 2 * i] := LowRes[j, i];
   end;


// Return whether a pixel with the specified neighbours can be skipped because
// of convexity. The neighbours should be listed in a circular order.
function IsConvex(
         a, b, c, d  :  Integer
         )           :  Integer;
   begin
   case ConvexAccel of
      0: Result := InvalidComponent;
      1: Result := SameComponent(a, b, c, d);
      2: Result := PickValidComponent(
            PickValidComponent(SameComponent(a, b, c), SameComponent(b, c, d)),
            PickValidComponent(SameComponent(c, d, a), SameComponent(d, a, b))
            );
      3: Result := PickValidComponent(SameComponent(a, c), SameComponent(b, d));
      else
         Assert(False);
      end;
   end;


// Fill the diagonal missing pixels of Amoeba
procedure FillPixelsDiag(
   var   Amoeba      :  TAmoeba;
   const Poly        :  TPoly;
   const DPolys      :  TPolys;
   const MaxP        :  TIntArray;
         R           :  Real;
         Epsilon     :  Real;
         NPoints,
         NAngles     :  Integer);
   var
         i, j, x, y,
         SX, SY, c   :  Integer;
         uv          :  TAmoebaCoords;
   begin
   GetSizes(SX, SY, Amoeba);
   for j := 0 to (SY - 1) div 2 - 1 do
      for i := 0 to (SX - 1) div 2 - 1 do
         begin
         x := 2 * i + 1;
         y := 2 * j + 1;
         c := IsConvex(Amoeba[y - 1, x - 1].Order1D,
                       Amoeba[y + 1, x - 1].Order1D,
                       Amoeba[y + 1, x + 1].Order1D,
                       Amoeba[y - 1, x + 1].Order1D);
         if c <> InvalidComponent then
            begin
            Amoeba[y, x].Order1D := c;
            Amoeba[y, x].NPoints := 0;
            end
         else
            begin
            GetPixelCoords(uv, x, y, SX, R);
            CalcAmoebaPixel(Amoeba[y, x], Poly, DPolys, uv, MaxP, Epsilon, NPoints, NAngles);
            end;
         end;
   end;


// Fill the orthogonal missing pixels of Amoeba. Must be preceded
// by FillPixelsDiag.
procedure FillPixelsOrthog(
   var   Amoeba      :  TAmoeba;
   const Poly        :  TPoly;
   const DPolys      :  TPolys;
   const MaxP        :  TIntArray;
         AmoebaR     :  Real;
         Epsilon     :  Real;
         NPoints,
         NAngles     :  Integer);
   var
         x, y,
         SX, SY, c,
         L, R, T, B  :  Integer;
         uv          :  TAmoebaCoords;
   begin
   GetSizes(SX, SY, Amoeba);
   for y := 0 to SY - 1 do
      for x := 0 to SX - 1 do
         if (x + y) mod 2 = 1 then
            begin
            if x > 0 then
               L := Amoeba[y, x - 1].Order1D else
               L := AmoebaInterior;
            if x < SX - 1 then
               R := Amoeba[y, x + 1].Order1D else
               R := AmoebaInterior;
            if y > 0 then
               B := Amoeba[y - 1, x].Order1D else
               B := AmoebaInterior;
            if y < SY - 1 then
               T := Amoeba[y + 1, x].Order1D else
               T := AmoebaInterior;
            c := IsConvex(T, R, B, L);
            if c <> InvalidComponent then
               begin
               Amoeba[y, x].Order1D := c;
               Amoeba[y, x].NPoints := 0;
               end
            else
               begin
               GetPixelCoords(uv, x, y, SX, AmoebaR);
               CalcAmoebaPixel(Amoeba[y, x], Poly, DPolys, uv, MaxP, Epsilon, NPoints, NAngles);
               end;
            end;
   end;


// Calculate the amoeba of Poly using the multiresolution algorithm.
procedure CalcAmoebaMultires(
   var   Amoeba   :  TAmoeba;
   const Poly     :  TPoly;
         R        :  Real;
         ImgSize  :  Integer;
         Epsilon  :  Real;
         NPoints,
         NAngles  :  Integer);
   var
         LowRes   :  TAmoeba;
         MaxP     :  TIntArray;
         DPolys   :  TPolys;
         uv       :  TAmoebaCoords;
         OldExMask:  TFPUExceptionMask;
   begin
   CalcAmoeba(Amoeba, Poly, R, {ImgSize:} 2, Epsilon, NPoints, NAngles);
   FindMaxPowers(MaxP, Poly);
   FullDiff(DPolys, Poly);
   OldExMask := GetExceptionMask();
   SetExceptionMask(OldExMask + [exUnderflow, exOverflow, exPrecision, exInvalidOp]);
   repeat
      CopyAmoeba(LowRes, Amoeba);
      UpscaleAmoeba(Amoeba, LowRes);
      FillPixelsDiag  (Amoeba, Poly, DPolys, MaxP, R, Epsilon, NPoints, NAngles);
      FillPixelsOrthog(Amoeba, Poly, DPolys, MaxP, R, Epsilon, NPoints, NAngles);
   until (Length(Amoeba) / ImgSize) > Sqrt(1 / 2);
   SetExceptionMask(OldExMask);
   end;

{-----------------------<< Speed and Acuraccy Tests >>-------------------------}

// Return time difference in seconds
function DeltaTime(
   const StartTime,
         StopTime    :  TDateTime
         )           :  Real;
   begin
   Result := 24 * 60 * 60 * (StopTime - StartTime);
   end;
   

// Run a full-factorial experiment to determine the acuraccy and performance of
// various parameter combinations. Amoeba images are saved to the working 
// folder.
procedure TestAcuraccy(
   const PathStats      :  AnsiString;
   const Poly           :  TPoly;
         R              :  Real;
         MaxLog2Eps,
         MaxLog2N,
         MaxLogNAngles,
         ImgSize        :  Integer);
   var
         FileStats      :  Text;
         Amoeba,  
         TrueAmoeba     :  TAmoeba;
         i, j, k        :  Integer;
         StartTime,  
         StopTime       :  TDateTime;
   begin
   CalcAmoebaMultires(TrueAmoeba, Poly, R, ImgSize, 
      1 / (1 shl MaxLog2Eps), 1 shl MaxLog2N, 1 shl MaxLogNAngles);
   SaveAmoeba('TrueAmoeba', TrueAmoeba, {SaveFullData:} False);
   OpenWrite(FileStats, PathStats);
   for k := 1 to MaxLogNAngles do
      for i := 2 to MaxLog2N do
         for j := 1 to MaxLog2Eps do
            begin
            StartTime := Now();
            CalcAmoebaMultires(Amoeba, Poly, R, ImgSize, 
               1 / (1 shl j), 1 shl i, 1 shl k);
            SaveAmoeba(Format('Amoeba_{}_{}_{}', [k, i, j]), 
               Amoeba, {SaveFullData:} False);
            StopTime := Now();
            WriteLn(FileStats,
               k,       Tab, i,        Tab, j,          Tab,
               TotalIntPoints(Amoeba)                 , Tab,
               AmoebaDifference(Amoeba, TrueAmoeba)   , Tab,
               DeltaTime(StartTime, StopTime)         );
            end;
   Close(FileStats);
   end;


procedure TestScaling(
   const PathStats      :  AnsiString;
   const Poly           :  TPoly;
         R              :  Real;
         MaxLog2ImgSize :  Integer;
         Epsilon        :  Real;
         NPoints,
         NAngles        :  Integer);
   var
         FileStats   :  Text;
         Amoeba      :  TAmoeba;
         i, k        :  Integer;
         StartTime,
         StopTime    :  TDateTime;
   begin
   OpenWrite(FileStats, PathStats);
   for k := 0 to 1 do
      for i := 1 to MaxLog2ImgSize do
         begin
         StartTime := Now();
         case k of
            0: CalcAmoeba        (Amoeba, Poly, R, 1 + 1 shl i, Epsilon, NPoints, NAngles);
            1: CalcAmoebaMultires(Amoeba, Poly, R, 1 + 1 shl i, Epsilon, NPoints, NAngles);
            end;
         StopTime := Now();
         WriteLn(FileStats,
            i,             Tab, k,                   Tab,
            TotalIntPoints(Amoeba)                 , Tab,
            DeltaTime(StartTime, StopTime)         );
         Flush(FileStats);
         end;
   Close(FileStats);
   end;


var
      Poly        :  TPoly;
      Amoeba      :  TAmoeba;
      StartTime,
      StopTime    :  TDateTime;
      Config      :  TIniConfig;
      LT          :  TLoadTable;
      Errors      :  TStringList;
      Params      :  TConfigParams;
const
      PathConfig  =  'Config.ini';
begin
// Load the config
LoadConfig(Config, PathConfig, Errors);
if Errors.N <> 0 then
{<}exit;
InitLoadTable(LT);
with Params do
   begin
   AddLoadParam(LT, PathPoly     , 'PathPoly'   , nil);
   AddLoadParam(LT, PathAmoeba   , 'PathAmoeba' , nil);
   AddLoadParam(LT, ImgSize      , 'ImgSize'    , nil);
   AddLoadParam(LT, RegionSize   , 'RegionSize' , nil);
   AddLoadParam(LT, NAngles      , 'NAngles'    , nil);
   AddLoadParam(LT, NIntPoints   , 'NIntPoints' , nil);
   AddLoadParam(LT, Epsilon      , 'Epsilon'    , nil);
   AddLoadParam(LT, WorkMode     , 'WorkMode'   , nil);
   LoadFromConfig(LT, Config, Errors);
   if Errors.N <> 0 then
{<}   exit;
         
   // Compute the amoeba or run the tests
   LoadPoly(Poly, PathPoly);
   case WorkMode of 
      'Normal': 
         begin
         StartTime := Now();
         CalcAmoebaMultires(Amoeba, Poly, RegionSize, ImgSize, Epsilon, NIntPoints, NAngles);
         StopTime := Now();
         WriteLn(DeltaTime(StartTime, StopTime) :6:2, ' seconds');
         SaveAmoeba('Amoeba', Amoeba, {SaveFullData:} True);
         ReadLn;
         end;
      'TestAcc': 
         TestAcuraccy('Stats.txt', Poly, RegionSize, 
            Round( -Log2(Epsilon   ) ), 
            Round(  Log2(NIntPoints) ), 
            Round(  Log2(NAngles   ) ), ImgSize);
      'TestScaling': 
         TestScaling('Stats.txt', Poly, RegionSize, 
            Round( Log2(ImgSize) ),  Epsilon, NIntPoints, NAngles);
      end;
   end;
end.
